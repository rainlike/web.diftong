<?php
/**
 * Menu Service
 * @TODO: description
 *
 * @package App\Service
 * @version 1.0.0
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 * @copyright 2018 spbcrew.com (https://www.spbcrew.com)
 * @author Alexander Saveliev <alex@spbcrew.com>
 */
declare(strict_types=1);

namespace App\Service;

/** Class Menu */
class Menu
{
    /**
     * Menu constructor
     */
    public function __construct() {}
}
